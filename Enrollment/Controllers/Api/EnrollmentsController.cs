﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Enrollment.Entities;
using Enrollment.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Enrollment.Controllers.Api
{
    [Route("api/v1")]
    [ApiController]
    public class EnrollmentsController : ControllerBase
    {
        private readonly IEnrollmentsRepository _repo;

        public EnrollmentsController(IEnrollmentsRepository enrollmentsRepository) => _repo = enrollmentsRepository;

        [HttpGet("enrollment/{id:int}")]
        public async Task<IActionResult> GetEnrollmentAsync(int id) => Ok(await _repo.GetAsync(id));

        [HttpPost("enrollment")]
        public async Task<IActionResult> PostEnrollmentAsync([FromBody]Person person)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var savedPerson = await _repo.AddOrUpdateAsync(person);
            if (savedPerson != null)
            {
                return Ok(savedPerson);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}