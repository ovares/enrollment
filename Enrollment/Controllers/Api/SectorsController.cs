﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Enrollment.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Enrollment.Controllers.Api
{
    [Route("api/v1")]
    [ApiController]
    public class SectorsController : ControllerBase
    {
        private readonly ISectorsRepository _repo;

        public SectorsController(ISectorsRepository sectorsRepository) => _repo = sectorsRepository;

        [HttpGet("sectors")]
        [ResponseCache(NoStore = true)]
        public async Task<IActionResult> GetSectorsAsync() => Ok(await _repo.GetAllAsync());
    }
}