﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Enrollment.Controllers.Api
{
    [Route("api/v1")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        [HttpGet("sessionid")]
        public IActionResult GetSessionid() => Ok(new { Sessionid = Guid.NewGuid().ToString() });
    }
}