SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Person](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Sectors] [nvarchar](max) NOT NULL,
	[Agreement] [bit] NOT NULL,
	[Sessionid] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

CREATE TABLE [dbo].[Sector](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SectorId] [nvarchar](max) NOT NULL,
	[SectorName] [nvarchar](max) NOT NULL,
	[GroupLevel] [int] NOT NULL,
 CONSTRAINT [PK_Sector] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

insert into Sector (SectorId, Sectorname, GroupLevel) values ('1', 'Manufacturing', 1)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('19', 'Construction materials', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('18', 'Electronics and Optics', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('6', 'Food and Beverage', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('342', 'Bakery & confectionery products', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('43', 'Beverages', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('42', 'Fish & fish products ', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('40', 'Meat & meat products', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('39', 'Milk & dairy products ', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('437', 'Other', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('378', 'Sweets & snack food', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('13', 'Furniture', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('389', 'Bathroom/sauna ', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('385', 'Bedroom', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('390', 'Children�s room ', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('98', 'Kitchen ', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('101', 'Living room ', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('392', 'Office', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('394', 'Other (Furniture)', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('341', 'Outdoor ', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('99', 'Project furniture', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('12', 'Machinery', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('94', 'Machinery components', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('91', 'Machinery equipment/tools', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('224', 'Manufacture of machinery ', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('97', 'Maritime', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('271', 'Aluminium and steel workboats ', 4)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('269', 'Boat/Yacht building', 4)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('230', 'Ship repair and conversion', 4)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('93', 'Metal structures', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('508', 'Other', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('227', 'Repair and maintenance service', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('11', 'Metalworking', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('67', 'Construction of metal structures', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('263', 'Houses and buildings', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('267', 'Metal products', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('542', 'Metal works', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('75', 'CNC-machining', 4)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('62', 'Forgings, Fasteners ', 4)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('69', 'Gas, Plasma, Laser cutting', 4)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('66', 'MIG, TIG, Aluminum welding', 4)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('9', 'Plastic and Rubber', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('54', 'Packaging', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('556', 'Plastic goods', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('559', 'Plastic processing technology', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('55', 'Blowing', 4)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('57', 'Moulding', 4)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('53', 'Plastics welding and processing', 4)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('560', 'Plastic profiles', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('5', 'Printing ', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('148', 'Advertising', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('150', 'Book/Periodicals printing', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('145', 'Labelling and packaging printing', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('7', 'Textile and Clothing', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('44', 'Clothing', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('45', 'Textile', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('8', 'Wood', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('337', 'Other (Wood)', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('51', 'Wooden building materials', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('47', 'Wooden houses', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('3', 'Other', 1)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('37', 'Creative industries', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('29', 'Energy technology', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('33', 'Environment', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('2', 'Service', 1)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('25', 'Business services', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('35', 'Engineering', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('28', 'Information Technology and Telecommunications', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('581', 'Data processing, Web portals, E-marketing', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('576', 'Programming, Consultancy', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('121', 'Software, Hardware', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('122', 'Telecommunications', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('22', 'Tourism', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('141', 'Translation services', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('21', 'Transport and Logistics', 2)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('111', 'Air', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('114', 'Rail', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('112', 'Road', 3)
insert into Sector (SectorId, Sectorname, GroupLevel) values ('113', 'Water', 3)

