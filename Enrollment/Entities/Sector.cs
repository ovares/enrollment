﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enrollment.Entities
{
    public class Sector
    {
        public int Id { get; set; }
        public string SectorId { get; set; }
        public string SectorName { get; set; }
        public int GroupLevel { get; set; }
    }
}
