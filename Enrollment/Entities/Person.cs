﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enrollment.Entities
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<string> Sectors { get; set; }
        public Boolean Agreement { get; set; }
        public string Sessionid { get; set; }
    }
}
