﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Newtonsoft.Json;

namespace Enrollment.Entities
{
    public class EnrollmentDbContext : DbContext
    {
        public EnrollmentDbContext(DbContextOptions<EnrollmentDbContext> options) : base(options)
        {
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Sector> Sectors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var converter = new ValueConverter<List<string>, string>(
                v => JsonConvert.SerializeObject(v), v => JsonConvert.DeserializeObject<List<string>>(v));

            modelBuilder.Entity<Person>().ToTable("Person").Property(p => p.Sectors).HasConversion(converter);
            modelBuilder.Entity<Sector>().ToTable("Sector");
        }
    }
}
