﻿using Enrollment.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enrollment.Repositories
{
    public class SectorsRepository : ISectorsRepository
    {
        private readonly EnrollmentDbContext _db;

        public SectorsRepository(EnrollmentDbContext context) => _db = context;

        public async Task<IEnumerable<Sector>> GetAllAsync() => (await _db.Sectors.ToListAsync());
    }
}
