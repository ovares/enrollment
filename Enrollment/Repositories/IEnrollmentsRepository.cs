﻿using Enrollment.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enrollment.Repositories
{
    public interface IEnrollmentsRepository
    {
        Task<Person> GetAsync(int id);
        Task<Person> AddOrUpdateAsync(Person person);
    }
}
