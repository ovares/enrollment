﻿using Enrollment.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enrollment.Repositories
{
    public interface ISectorsRepository
    {
        Task<IEnumerable<Sector>> GetAllAsync();
    }
}
