﻿using Enrollment.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Enrollment.Repositories
{
    public class EnrollmentsRepository : IEnrollmentsRepository
    {
        private readonly EnrollmentDbContext _db;

        public EnrollmentsRepository(EnrollmentDbContext context) => _db = context;

        public async Task<Person> GetAsync(int id) => (await _db.Persons.FirstOrDefaultAsync(p => p.Id == id));

        public async Task<Person> AddOrUpdateAsync(Person person)
        {
            var personInDb = await _db.Persons.FirstOrDefaultAsync(p => p.Sessionid == person.Sessionid);
            if (personInDb != null)
            {
                personInDb.Name = person.Name;
                personInDb.Sectors = person.Sectors;
                await _db.SaveChangesAsync();
                return personInDb;
            }
            else
            {
                await _db.Persons.AddAsync(person);
                await _db.SaveChangesAsync();
                return person;
            }
        }
    }
}
