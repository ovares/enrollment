﻿var app = angular.module('enrollmentApp', []);

app.controller('EnrollmentsController', function ($scope, $http) {
    if (!sessionStorage.getItem("sessionid")) {
        $http.get('/api/v1/sessionid').success(function (response) {
            sessionStorage.setItem("sessionid", response.sessionid);
        });
    }
    
    $http.get('/api/v1/sectors').success(function (response) {
        $scope.sectors = response;
    });

    $scope.save_person = function () {
        if (!$scope.enrollmentForm.$invalid) {
            $scope.person.sessionid = sessionStorage.getItem("sessionid");
            $http.post('/api/v1/enrollment/', $scope.person).success(function (response) {
                $scope.person = response;
                alert('Your data has been saved.');
            });
        }
    };
});